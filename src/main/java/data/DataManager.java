package data;

public enum  DataManager {
    EMAIL("email@gmail.com"),
    PASSWORD("pasword"),
    PRODUCT("Bilgisayar"),
    PAGE_NUMBER("2"),
    INCREASE_NUMBER("2")
    ;

    private final String data;

    DataManager(String data)
    {
        this.data = data;
    }

    public String getData()
    {
        return data;
    }

    public int getDataInt()
    {
        try {
            return Integer.parseInt(data);
        }catch (Exception e){
            return -1;
        }
    }

}