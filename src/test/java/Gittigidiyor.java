import base.BaseTest;
import data.DataManager;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pages.BasketPage;
import pages.HomePage;
import pages.LoginPage;
import pages.ProductPage;

import static utils.AutomationProcess.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Gittigidiyor extends BaseTest {

    @Test
    public void stage1_openWebsite() {
        WebTest_Automation_Task_Test(1, "Open Website");
        WebTest_Automation_Task_Create_Page("Open Website");
        HomePage homePage = new HomePage();

        homePage.openPage();
        homePage.checkItOpen();
        WebTest_Automation_Task_Finish_Page("Open Website");
    }

    @Test
    public void stage2_loginWebsite() {
        WebTest_Automation_Task_Test(2, "Login Website");
        WebTest_Automation_Task_Create_Page("Login Website");
        HomePage homePage = new HomePage();
        LoginPage loginPage = new LoginPage();

        homePage.clickLogin();
        loginPage.setUsername(DataManager.EMAIL.getData());
        loginPage.setPassword(DataManager.PASSWORD.getData());
        loginPage.clickLogin();
        homePage.checkLogin();
        WebTest_Automation_Task_Finish_Page("Login Website");
    }

    @Test
    public void stage3_searchProduct() {
        WebTest_Automation_Task_Test(3, "Search Product");
        WebTest_Automation_Task_Create_Page("Search Product");
        HomePage homePage = new HomePage();
        ProductPage productPage = new ProductPage();

        homePage.searchProduct(DataManager.PRODUCT.getData());
        homePage.goPageFromResult(DataManager.PAGE_NUMBER.getDataInt());
        homePage.checkOpenNewPage(DataManager.PAGE_NUMBER.getDataInt());
        homePage.selectRandomProduct();
        productPage.getProductInfo();
        productPage.addProductToBasket();
        WebTest_Automation_Task_Finish_Page("Search Product");
    }

    @Test
    public void stage4_cartOperations() {
        WebTest_Automation_Task_Test(4, "Cart Operations");
        WebTest_Automation_Task_Create_Page("Cart Operations");
        HomePage homePage = new HomePage();
        BasketPage basketPage = new BasketPage();

        homePage.openBasketPage();
        basketPage.checkProductInfo();
        basketPage.increaseProductNumber(DataManager.INCREASE_NUMBER.getDataInt());
        basketPage.checkProductNumber(DataManager.INCREASE_NUMBER.getDataInt());
        basketPage.removeProduct();
        basketPage.checkEmptyBasket();
        WebTest_Automation_Task_Finish_Page("Cart Operations");
    }

}
